<!-- NOTE: If your issue is a security concern, please send an email to security@gitea.io and (!) contact@codeberg.org instead of opening a public issue -->

<!-- Codeberg contributions: Please read this!
Thank you for opening an issue. If you want to report a regular bug or open a feature request, please either use the Codeberg Community Issue tracker at https://codeberg.org/Codeberg/Community/issues or directly report to upstream instead. Please also search for duplicates in these places.

Please only use this bug tracker for the small amount of bugs that are directly related to our fork or to ask questions on how to contribute to Codeberg (we'll gladly answer your questions here). For the majority of reports and feature requests, the community issue tracker is the better place - it's monitored by more users and has got more structure.

Thank you.
-->

- Git version:
- Operating system:
- Can you reproduce the bug at https://try.gitea.io:
  - [ ] Yes (provide example URL)
  - [ ] No

## Description

...


## Screenshots

<!-- **If this issue involves the Web Interface, please include a screenshot** -->
